package com.redono.coulomb;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CoulombFrame extends JFrame {

	public CoulombFrame(){
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(new CoulombComponent(), BorderLayout.CENTER);
		add(panel);
	}
}
