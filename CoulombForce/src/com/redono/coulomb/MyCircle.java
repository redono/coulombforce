package com.redono.coulomb;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

public class MyCircle
{
	private int elementaryCharge;
	private static final double CIRCLE_RADIUS = 5.0;
	private Ellipse2D circle;
	public MyCircle(double x, double y, int charge){
		circle = new Ellipse2D.Double(x-CIRCLE_RADIUS, y-CIRCLE_RADIUS, CIRCLE_RADIUS*2, CIRCLE_RADIUS*2);
		elementaryCharge = charge;
	}
	
	public Ellipse2D getCircle(){
		return (new Ellipse2D.Double(circle.getX(), circle.getY(), circle.getWidth(), circle.getHeight()));
	}
	
	public void setFrame(double x, double y){
		circle.setFrame(x-CIRCLE_RADIUS, y-CIRCLE_RADIUS, CIRCLE_RADIUS*2, CIRCLE_RADIUS*2);
	}
	
	public boolean contains(Point2D p){
		return circle.contains(p);
	}
	
	public void addCharge(int i){
		elementaryCharge += i;
	}
	
	public double getX(){
		return circle.getCenterX();
	}
	
	public double getY(){
		return circle.getCenterY();
	}
	
	public double getCharge(){
		return (double)elementaryCharge;
	}
}
