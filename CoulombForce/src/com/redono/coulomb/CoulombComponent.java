package com.redono.coulomb;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JComponent;
public class CoulombComponent extends JComponent {
	
	private ArrayList<MyCircle> circles;
	private MyCircle current;
	private String calculationString;
	
	public CoulombComponent()
	{
		calculationString = "Click to draw circles and you will see the force between them.";
		circles = new ArrayList<>(2);
		circles.add(null);
		circles.add(null);
		
		
		addMouseListener(new MouseAdapter(){

			public void mousePressed(MouseEvent event){
				int charge = 0;
				if ((MouseEvent.BUTTON1_DOWN_MASK & event.getModifiersEx()) != 0){
					charge = 1;
				}
				if ((MouseEvent.BUTTON3_DOWN_MASK & event.getModifiersEx()) != 0){
					charge = -1;
				}
				current = find(event.getPoint());
				if(current == null){
					add(event.getPoint(), charge);
				} else {current.addCharge(charge);}
				reCalculate();
			}
				
			public void mouseClicked(MouseEvent event){
				current = find(event.getPoint());
				if(current != null && event.getClickCount() >= 4){
					remove(current);
				}
			}
		});

		addMouseMotionListener(new MouseMotionListener(){
			public void mouseMoved(MouseEvent event){
				;
			}
			public void mouseDragged(MouseEvent event){
				if (current != null){
					double x = event.getX();
					double y = event.getY();
					
					current.setFrame(x,y);
					reCalculate();
				}
			}
			
		});
	}
	
	public MyCircle find(Point2D p){
		for(MyCircle c : circles){
			if(c!=null){
				if(c.contains(p)){
					return c;
				}
			}
		} 
		return null;
	}
	
	public void remove(MyCircle c){
		if (c==null){
			return;
		}
		if (c == current){
			current = null;
		}
		circles.set(circles.indexOf(c), null);
		reCalculate();
		
	}
	
	public void add(Point2D p, int charge){
		double x = p.getX();
		double y = p.getY();
		current = new MyCircle(x, y, charge);
		circles.set(0, circles.get(1));
		circles.set(1, current);
	}
		
	private void reCalculate(){
		MyCircle c1 = circles.get(0);
		MyCircle c2 = circles.get(1);
		if (c1!=null && c2!=null){
			double charge = ((circles.get(0).getCharge() * circles.get(1).getCharge()));
			double distance = Math.sqrt(Math.pow(c1.getX() - c2.getX(), 2.0)+Math.pow(c1.getY() - c2.getY(), 2.0));
			double force = charge/(distance*distance);
			double largerForce = force*1000;
			DecimalFormat df = new DecimalFormat("#.###");
			String forceAsString = df.format(largerForce);
			calculationString = "The force between these two objects is "+forceAsString+".";
		} else {calculationString = "There is no force apparent.";}
		repaint();
	}
	
	
	public void paintComponent(Graphics g){
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawString(calculationString, 25, 25);
		for(MyCircle c:circles){
			if (c!= null){
				g2d.drawString("The strength of the " + (circles.indexOf(c)==0?"first":"second") + " is " + (int)c.getCharge() + "e.",
						25,
						50+(25*circles.indexOf(c)));
				g2d.draw(c.getCircle());
			}
		}
		
		if (circles.get(0)!=null && circles.get(1)!= null){
			g2d.setColor(Color.RED);
			g2d.drawLine((int)circles.get(0).getX(), (int)circles.get(0).getY(), (int)circles.get(1).getX(), (int)circles.get(1).getY());
			g2d.setColor(Color.RED);
		}
		
	}
	
}

