package com.redono.coulomb;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class CoulombMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable(){
			public void run(){
				CoulombFrame frame = new CoulombFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(800,800);
				frame.setVisible(true);
			}
		});
	}

}
